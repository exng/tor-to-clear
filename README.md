# tor-to-clear

Reverse proxy a hidden service to clear web.

## Usage

**Requirements:**

-   python v^3.7.4
-   rsocks v^0.3.3
-   tor v^0.4.1.5

```bash
$ ./ttc/start.py <onion_address> port[,...]
# or use the provided docker image
$ docker run -d --name tor-to-clear \
  --restart unless-stopped \
  -p 80:80 \
  registry.gitlab.com/exng/tor-to-clear:latest \
  xxx.onion [<host_port>[:<remote_port>] ...]
```

## License

MIT License

Copyright (c) 2019 Johann Behr
