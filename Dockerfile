FROM python:latest

# Install tor
RUN apt-get update && \
  apt-get upgrade -y &&  \
  apt-get install -y tor

# Install rsocks
RUN pip install rsocks

COPY ./ttc/. /var/ttc

ENTRYPOINT ["/var/ttc/entry.sh"]
