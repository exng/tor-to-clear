#!/bin/bash
# exit if command fails
set -e
set -x

# start tor with custom torrc
tor -f /var/ttc/torrc &

# start python reverse proxy
python /var/ttc/start.py $@
