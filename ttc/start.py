from os import environ
from sys import argv
from rsocks.pool import ServerPool
from rsocks.server import ReverseProxyServer

# get proxy address
proxy = environ.get('SOCKS_PROXY', 'SOCKS5://localhost:9050')
pool = ServerPool()

# reverse proxy all passed ports from target url
target_url = str(argv[1])
# loop over ports
for port in argv[2:]:
    port_parts = port.split(":")
    host_port = port_parts[0]

    try:
        remote_port = port_parts[1]
    except IndexError:
        remote_port = host_port

    with pool.new_server(
            name=str(port),
            server_class=ReverseProxyServer,
            upstream=(target_url, int(remote_port))) as server:
        server.set_proxy(proxy)
        server.listen(('0.0.0.0', int(host_port)))

if __name__ == '__main__':
    # start reverse proxy only if run indepently, not as module
    pool.loop()
